
import type { Parking } from "../features/spotSelector/parkingSpotSlice";

import { select } from "../features/spotSelector/parkingSpotSlice";
import { useParkDispatch } from "../utils/hooks";

export function ParkingSpots ({ allSpots }: { allSpots: Parking.Spots; }) {
  const dispatch = useParkDispatch();

  // determine parking space size
  const [ maxX, maxY ] = allSpots.reduce(function ( [ mx, my, ], { x, y, } ) {
    return [ Math.max(mx, x), Math.max(my, y), ];
  }, [ 0, 0, ]);

  return (
    <div className="parking__lot" style={{
      gridTemplateRows: `repeat(${maxY}, 1fr)`,
      gridTemplateColumns: `repeat(${maxX + 2}, 1fr)`,
    }}>
      {new Array(maxY + 1).fill(0).flatMap(function ( _, index ) {
        const letter = String.fromCharCode("A".charCodeAt(0) + index);

        return [
          // left buildings
          (<div onClick={( ) => dispatch(select({ x: 0, y: index, }))}
            className="building__box"
            style={{ gridColumn: 1, gridRow: index + 1, }} key={`${letter}1`}>
            {letter}1
          </div>
          ),
          // right buildings
          (<div onClick={( ) => dispatch(select({ x: maxX + 2, y: index, }))}
            className="building__box"
            style={{ gridColumn: maxX + 3, gridRow: index + 1, }} key={`${letter}2`}>
            {letter}2
          </div>
          ),
        ];
      })}
      {allSpots.map(function ( { x, y, taken }, indexRow ) {
        const classes = [ "parking__spot", taken && "parking__spot--taken", ]
          .filter(Boolean).join(" ");

        return (
          <div className={classes} key={indexRow}
            style={{ gridColumn: x + 2, gridRow: y + 1 }} />
        );
      })}
    </div>
  );
};
