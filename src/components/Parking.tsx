
import { ParkingSpots } from "./ParkingSpots";
import { reset } from "../features/spotSelector/parkingSpotSlice";
import { useParkSelector, useParkDispatch, } from "../utils/hooks";

export function Parking ( ) {
  const allSpots = useParkSelector(( state ) => { return state.parkingSpots });
  const dispatch = useParkDispatch();

  return (
    <div className="App">
      <div>parking: </div>
      <div className="parking__reset" onClick={( ) => dispatch(reset())}>
        Reset
      </div>
      <ParkingSpots allSpots={allSpots} />
    </div>
  );
};
