
import { Parking } from "./components/Parking";
import React from "react";

function App ( ) {
  return (
    <div className="App">
      <Parking />
    </div>
  );
}

export default App;
