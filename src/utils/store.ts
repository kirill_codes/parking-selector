
import { configureStore } from "@reduxjs/toolkit";
import parkingSpotReducer from "../features/spotSelector/parkingSpotSlice";

import storage from "redux-persist/lib/storage";
import { persistReducer, persistStore, PERSIST, } from "redux-persist";

const persistConfig = {
  key: "root", storage,
};

const persistedReducer = persistReducer(persistConfig, parkingSpotReducer);

export const store = configureStore({
  reducer: persistedReducer,

  middleware: ( getDefaultMiddleware ) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [ PERSIST, ],
      },
    }),
});
export const persistor = persistStore(store);

export type StoreDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
