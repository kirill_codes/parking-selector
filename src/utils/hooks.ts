import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { RootState, StoreDispatch } from "./store";

export const useParkDispatch = ( ) => useDispatch<StoreDispatch>();
export const useParkSelector: TypedUseSelectorHook<RootState> = useSelector;
