
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export namespace Parking {
  export interface Location {
    x: number; y: number;
  }

  export interface Spot extends Location {
    taken: boolean;
  }

  export type Spots = Array<Spot>;

  export interface State {
    parkingSpots: Array<Spot>;
  }
}

const initialState: Parking.State = {
  parkingSpots: ([ 5, 2, 5, 3, 3, 5 ]).flatMap(function ( length, y ) {
    return new Array(length).fill(0).map(function ( _tuple, x ) {
      return { x, y, taken: false, };
    });
  }),
};

const parkingSpotSlice = createSlice({
  name: "parkingSpots",
  initialState,
  reducers: {
    select ( state, { payload: { x, y }, }: PayloadAction<Parking.Location> ) {
      const result = state.parkingSpots.filter(function ( spot ) {
        return !spot.taken;
      }).map(function ( spot ) {
        return [ spot, Math.sqrt(Math.pow(spot.x - x, 2) + Math.pow(spot.y - y, 2)), ] as const;
      }).sort(function ( a, b ) {
        return a[1] - b[1];
      });

      if (result?.length >= 1) {
        result[0][0].taken = true;
      } else {
        window.alert("No more parking space available! Check back in later.")
      }
    },
    reset ( state, ) { state.parkingSpots = initialState.parkingSpots; }
  }
});

export const { select, reset, } = parkingSpotSlice.actions;
export default parkingSpotSlice.reducer;
