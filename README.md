
## About

Parking spot Selector.
Technologies used: React.js, Redux toolkit, TypeScript, react-scripts

## To run locally:

```sh
npm install
npm run start
```

## Notes:

- Each time user selects a house - the closest by Euclidian distance parking spot will be selected an occupied by the user.
- Project uses Redux for persisting data to remove a few layers of complexity linked to implementing the db / backend layer.
- Implementation includes assumptions such as:
  - number of buildings matches the number of parking rows


## Some ideas for extra feautures:
 - optimize algo for more accurate calculation
 - improve display
 - add animations etc.
